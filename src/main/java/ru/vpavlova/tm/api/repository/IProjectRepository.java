package ru.vpavlova.tm.api.repository;

import ru.vpavlova.tm.api.IBusinessRepository;
import ru.vpavlova.tm.entity.Project;

public interface IProjectRepository  extends IBusinessRepository<Project> {

}
